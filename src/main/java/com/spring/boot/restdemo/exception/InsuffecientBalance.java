package com.spring.boot.restdemo.exception;

public class InsuffecientBalance extends RuntimeException{
    public InsuffecientBalance(String msg) {
        super(msg);
    }

}
