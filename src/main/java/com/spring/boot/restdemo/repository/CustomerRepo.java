package com.spring.boot.restdemo.repository;

import com.spring.boot.restdemo.dto.OrderResponse;
import com.spring.boot.restdemo.entity.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface CustomerRepo extends JpaRepository<Customer,Integer> {
    @Query("SELECT new com.spring.boot.restdemo.dto.OrderResponse(C.customerName,P.productName) FROM Customer C JOIN C.products P")
 public List<OrderResponse> getJoinInformation();
}
