package com.spring.boot.restdemo.repository;

import com.spring.boot.restdemo.entity.Product;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductRepo extends JpaRepository<Product,Integer> {
    Product findByproductName(final String productName);
}
