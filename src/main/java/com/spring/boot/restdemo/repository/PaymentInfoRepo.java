package com.spring.boot.restdemo.repository;

import com.spring.boot.restdemo.entity.PaymentInfo;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PaymentInfoRepo extends JpaRepository<PaymentInfo,String> {
}
