package com.spring.boot.restdemo.repository;

import com.spring.boot.restdemo.entity.PassengerInfo;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PassengerInfoRepo extends JpaRepository<PassengerInfo,Long> {
}
