package com.spring.boot.restdemo.utils;

import com.spring.boot.restdemo.exception.InsuffecientBalance;

import java.util.HashMap;
import java.util.Map;

public class PaymentUtils {

    private static Map<String,Double> paymentMap=new HashMap<>();

    static {
        paymentMap.put("acc1",12000.0);
        paymentMap.put("acc2",15000.0);
        paymentMap.put("acc3",5000.0);
        paymentMap.put("acc4",8000.0);
    }
    public static boolean validateCreditLimit(String accountNo,double paidAmount){

        if(paidAmount>paymentMap.get(accountNo)){

        throw new InsuffecientBalance("Insuffecient amount to process this request");
        }else{
            return true;
        }
    }
}
