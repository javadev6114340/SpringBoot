package com.spring.boot.restdemo;

import com.spring.boot.restdemo.dto.FlightBookingAcknoledgmnt;
import com.spring.boot.restdemo.dto.FlightBookingRequest;
import com.spring.boot.restdemo.service.FlightBookingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
//@RestController
@EnableTransactionManagement
public class RestdemoApplication {
	/* Eaxmple of transaction management with scenerio of flight booking
	@Autowired
	private FlightBookingService flightBookingService;
	@PostMapping("/bookFlightTicket")

	public FlightBookingAcknoledgmnt bookFlightTicket(@RequestBody FlightBookingRequest flightBookingRequest){
		return flightBookingService.bookFlightTicket(flightBookingRequest);
	}*/

	public static void main(String[] args) { SpringApplication.run(RestdemoApplication.class, args);
	}

}
