package com.spring.boot.restdemo.controller;

import com.spring.boot.restdemo.dto.OrderRequest;
import com.spring.boot.restdemo.dto.OrderResponse;
import com.spring.boot.restdemo.entity.Customer;
import com.spring.boot.restdemo.repository.CustomerRepo;
import com.spring.boot.restdemo.repository.ProductRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/*One to many example*/
@RestController
public class OrderController {
    @Autowired
    private CustomerRepo customerRepo;
    @Autowired
    private ProductRepo productRepo;

    @PostMapping("/placeOrder")
    public Customer placeOrder(@RequestBody OrderRequest orderRequest){
        return customerRepo.save(orderRequest.getCustomer());
    }
    @GetMapping("/findAllOrder")
    public List<Customer> findAllOrders(){
        return customerRepo.findAll();
    }


    @GetMapping("/findOrderWithJoin")
    public List<OrderResponse> findOrderWithJoin(){
        return customerRepo.getJoinInformation();
    }
}
