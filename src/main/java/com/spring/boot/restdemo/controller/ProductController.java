package com.spring.boot.restdemo.controller;

import com.spring.boot.restdemo.entity.Product;
import com.spring.boot.restdemo.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class ProductController {
    /*This controller will talk to service class,so
    * we will inject ProductService class
    * */
    @Autowired
    private ProductService productService;

    /**
     * Now we need to write REST Endpoint for all the methods in service
     * 1. Post method
     * 2. GET method
     */
    @PostMapping("/addProduct")
    public Product addProduct (@RequestBody Product product){
        return productService.saveProduct(product);
    }
    @PostMapping("/addProducts")
    public List<Product> addProducts (@RequestBody List<Product> product){
        return productService.saveProducts(product);
    }
    @GetMapping("/products")
    public List<Product> findAllProducts(){
        return productService.getProducts();
    }
    /**
     * @PathVariable--{id} must add in request url else you will get 404
     * @RequestParam
     * To Pass parameter
     */

    @GetMapping("/product/{id}")
    public Product findProductById(@PathVariable int id){
        return productService.getProductById(id);
    }
    @GetMapping("/product/{name}")
    public Product findProductByName(@RequestParam String productName){
        return productService.getProductByName(productName);
    }

    @PutMapping ("/updateProduct")
    public Product upadteProduct (@RequestBody Product product){
        return productService.saveProduct(product);
    }
    @DeleteMapping("/delete/{id}")
    public String deleteProduct(@PathVariable int id){
        return productService.deleteProduct(id);
    }
}
