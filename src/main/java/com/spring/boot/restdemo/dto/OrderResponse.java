package com.spring.boot.restdemo.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public class OrderResponse {
    private String customerName;
    private String productName;
    private String gender;

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public OrderResponse(String customerName, String productName) {
        this.customerName = customerName;
        this.productName = productName;
    }

    @Override
    public String toString() {
        return "OrderResponse{" +
                "customerName='" + customerName + '\'' +
                ", productName='" + productName + '\'' +
                '}';
    }

    public OrderResponse() {
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }
}
