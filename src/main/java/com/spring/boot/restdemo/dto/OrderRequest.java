package com.spring.boot.restdemo.dto;

import com.spring.boot.restdemo.entity.Customer;


public class OrderRequest {
    public Customer getCustomer() {
        return customer;
    }

    public OrderRequest() {
        super();
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    private Customer customer;

    public OrderRequest(Customer customer) {
        this.customer = customer;
    }

    @Override
    public String toString() {
        return "OrderRequest{" +
                "customer=" + customer +
                '}';
    }



}
