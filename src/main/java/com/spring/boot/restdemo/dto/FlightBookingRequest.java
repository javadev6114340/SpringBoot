package com.spring.boot.restdemo.dto;

import com.spring.boot.restdemo.entity.PassengerInfo;
import com.spring.boot.restdemo.entity.PaymentInfo;

public class FlightBookingRequest {
    public PassengerInfo getPassengerInfo() {
        return passengerInfo;
    }

    public void setPassengerInfo(PassengerInfo passengerInfo) {
        this.passengerInfo = passengerInfo;
    }

    public PaymentInfo getPaymentInfo() {
        return paymentInfo;
    }

    public void setPaymentInfo(PaymentInfo paymentInfo) {
        this.paymentInfo = paymentInfo;
    }

    private PassengerInfo passengerInfo;
    private PaymentInfo paymentInfo;
}
