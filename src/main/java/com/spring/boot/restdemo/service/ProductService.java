package com.spring.boot.restdemo.service;

import com.spring.boot.restdemo.entity.Product;
import com.spring.boot.restdemo.repository.ProductRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductService {
    //Inside this service we need to inject repo file--ProductRepo
    @Autowired
    private ProductRepo productRepo;

    public ProductService(ProductRepo productRepo) {
        this.productRepo = productRepo;
    }

    /*Now write all the http method
    * PUT,POST,GET,DELETE
    * To save records in db use POST
    * To get list of objects from db use GET
    * */

    public Product saveProduct(Product product){
        return productRepo.save(product);
    }
    //To save List of products
    public List<Product> saveProducts(List<Product> products){
        return productRepo.saveAll(products);
    }

    public List<Product> getProducts(){
        return productRepo.findAll();
    }
    /*
    * It will return option if there is a record else return null*/
    public Product getProductById(int id){
        return productRepo.findById(id).orElse(null);
    }
    /*
    * Our own customized method
    * */
    public Product getProductByName(String productName){
        return productRepo.findByproductName(productName);
    }

    public String deleteProduct(int id){
        productRepo.deleteById(id);
        return "Product removed";
    }
    /*
    * We don't have update method in spring jpa,so we will use save method
    * 1. Get product ny id which we want to update
    * 2. populate all the new values in existing object
    * 3. call save method with that object as input  */
    public Product updateProduct(Product product){
        Product existingProduct =productRepo.findById(product.getId()).orElse(null);
        assert existingProduct != null;
        existingProduct.setProductName(product.getProductName());
        existingProduct.setPrice(product.getPrice());
        existingProduct.setQuantity(product.getQuantity());
        return productRepo.save(existingProduct);
    }
}
