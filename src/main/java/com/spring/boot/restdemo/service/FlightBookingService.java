package com.spring.boot.restdemo.service;

import com.spring.boot.restdemo.dto.FlightBookingAcknoledgmnt;
import com.spring.boot.restdemo.dto.FlightBookingRequest;
import com.spring.boot.restdemo.entity.PassengerInfo;
import com.spring.boot.restdemo.entity.PaymentInfo;
import com.spring.boot.restdemo.repository.PassengerInfoRepo;
import com.spring.boot.restdemo.repository.PaymentInfoRepo;
import com.spring.boot.restdemo.utils.PaymentUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

@Service
public class FlightBookingService {
    @Autowired
    private PassengerInfoRepo passengerInfoRepo;
    @Autowired
    private PaymentInfoRepo paymentInfoRepo;

    /*
    * FlightBookingRequest : Contains PassengerInfo and PaymentInfo both*/
    @Transactional(readOnly=false,isolation = Isolation.READ_COMMITTED,propagation = Propagation.REQUIRED)
    public FlightBookingAcknoledgmnt bookFlightTicket(FlightBookingRequest flightBookingRequest){
        FlightBookingAcknoledgmnt acknoledgmnt =null;
        PassengerInfo passengerInfo =flightBookingRequest.getPassengerInfo();
        passengerInfo = passengerInfoRepo.save(passengerInfo);
        PaymentInfo paymentInfo =flightBookingRequest.getPaymentInfo();
        PaymentUtils.validateCreditLimit(paymentInfo.getAccountNo(),passengerInfo.getFare());
        paymentInfo.setPassengerId(passengerInfo.getpId());
        paymentInfo.setAmount(passengerInfo.getFare());
        paymentInfoRepo.save(paymentInfo);
        return new FlightBookingAcknoledgmnt("Success",passengerInfo.getFare(), UUID.randomUUID().toString().split("-")[0],passengerInfo);
    }

}
